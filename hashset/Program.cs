﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hashset
{
    class Program
    {
        static void Main(string[] args)
        {
            // SET = Conjuntos

            // Características que os distinguem das listas
            // 1. não permite registos duplos
            // 2. os elementos não são mantido por ordem específica (na lista um novo elemento fica no fim da lista)

            // Definição
            ISet<string> Alunos = new HashSet<string>();

            // adicionar alunos
            Alunos.Add("Sara Cardoso");
            Alunos.Add("Maria Carmo");
            Alunos.Add("Rui Matos");

            //foreach (var aluno in Alunos)
            //{
            //    Console.WriteLine(aluno);
            //}

            Console.WriteLine(string.Join(", ", Alunos));

            // adicionar mais alunos
            Alunos.Add("Susana Maria");
            Alunos.Add("Carlos do Carmo");
            Alunos.Add("Maria Joana");

            Console.WriteLine(string.Join(", ", Alunos));

            Alunos.Remove("Susana Maria");
            Alunos.Add("Rafael Matos"); // O Rafael fica no lugar da Susana | se fosse uma lista metia no fim

            Console.WriteLine(string.Join(", ", Alunos));

            Alunos.Add("Maria Carmo"); // retorna false e não faz nada porque Maria Carmo já existia

            Console.WriteLine(string.Join(", ", Alunos));

            // diferença de performance entre List e HashSet
            // O HashSet ganha em rapidez mas perde em memória

            // PARA ORDENAR, usamos uma lista só para isso
            Console.WriteLine("ORDENADO:");

            List<string> ListaAlunos = new List<string>(Alunos); // agarrar no conjunto e metê-lo numa lista
            ListaAlunos.Sort();
            Console.WriteLine(string.Join(", ",ListaAlunos));

            Console.ReadKey();



        }
    }
}
