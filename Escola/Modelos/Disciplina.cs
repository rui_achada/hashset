﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola.Modelos
{
    public class Disciplina
    {
        public string Titulo { get; set; }

        public int Tempo { get; set; }

        public Disciplina(string titulo, int tempo)
        {
            Titulo = titulo;

            Tempo = tempo;

        }

        public override string ToString() // sem isto ele mostra apenas o objeto (Escola.Modelos.Disciplina)
        {
            return $"Titulo: {Titulo} - Tempo: {Tempo} horas";
        }
    }
}
