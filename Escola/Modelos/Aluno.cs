﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola.Modelos
{
    public class Aluno
    {
        public string Nome { get; set; }

        public int Numero { get; set; }

        public Aluno(string nome, int numero)
        {
            Nome = nome;
            Numero = numero;
        }

        public override string ToString()
        {
            return $"Nome: {Nome}  Numero: {Numero}";
        }

        /// <summary>
        /// Vai comparar o nome das instâncias e não as instâncias em si
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            Aluno outroAluno = obj as Aluno;

            if (outroAluno == null)
            {
                return false;
            } 

            return this.Nome.Equals(outroAluno.Nome);
        }

        public override int GetHashCode()
        {
            return this.Nome.GetHashCode(); // dá o HashCode do Nome
        }
    }
}
