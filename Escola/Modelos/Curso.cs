﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola.Modelos
{
    public class Curso
    {
        // atributos
        private ISet<Aluno> ConjuntoDeAlunos = new HashSet<Aluno>();

        // Propriedades

        public string Nome { get; set; }

        public List<Disciplina> Disciplinas { get; set; }

        public IList<Aluno> Alunos
        {
            get
            {
                return new ReadOnlyCollection<Aluno>(ConjuntoDeAlunos.ToList());
            }
        }

        public Curso(string nome)
        {
            Nome = nome;

            Disciplinas = new List<Disciplina>();
        }

        internal void AdicionaDisciplinas(Disciplina disciplina)
        {
            Disciplinas.Add(disciplina);
        }

        internal void matricula(Aluno aluno)
        {
            ConjuntoDeAlunos.Add(aluno);
        }

        internal bool EstaMatriculado(Aluno aluno)
        {
            return ConjuntoDeAlunos.Contains(aluno); // retorna true ou false / aluno existe ou não
        }
    }
}
