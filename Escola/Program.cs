﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Escola.Modelos;

namespace Escola
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declarar o curso
            Curso tpsi = new Curso("Programação de Sistemas de Informação");

            // Adicionar 3 disciplinas
            tpsi.AdicionaDisciplinas(new Disciplina("Algoritmos", 25));
            tpsi.AdicionaDisciplinas(new Disciplina("Estruturada", 50));
            tpsi.AdicionaDisciplinas(new Disciplina("POO", 30));

            foreach (var disciplina in tpsi.Disciplinas)
            {
                Console.WriteLine(disciplina);
            }

            // Criar os alunos com nome e matrícula
            Aluno a1 = new Aluno("Vanessa Tires", 17889);
            Aluno a2 = new Aluno("Ana Luísa", 17454);
            Aluno a3 = new Aluno("Miguel Saraiva", 15323);

            // Matricular os alunos no curso
            tpsi.matricula(a1);
            tpsi.matricula(a2);
            tpsi.matricula(a3);

            Console.WriteLine("Alunos matriculados");
            foreach (var aluno in tpsi.Alunos)
            {
                Console.WriteLine(aluno);
            }

            // ------------------------ Comparações -------------------

            // A Vanessa está matriculada ?
            Console.WriteLine(tpsi.EstaMatriculado(a1)); // true

            // Agora vai dar falso porque estamos a comparar duas instâncias diferentes
            Aluno outroAluno = new Aluno("Vanessa Tires", 17889);
            Console.WriteLine(tpsi.EstaMatriculado(outroAluno)); // depois de fazer override do HashCode, passou a true

            Console.WriteLine(a1 == outroAluno); // false - duas instâncias diferentes também

            Console.WriteLine(a1.Equals(outroAluno)); // false (vamos fazer override do equals em Aluno.cs)
            // depois do override já retorna true



            Console.ReadKey();
        }
    }
}
